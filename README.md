# Sentiment with Node.js

### Prepare to run
- Run your database server (XAMPP)
- Create database on your local server
- Import file sql in this project to your database
- Setting at file .env
- Edit on DB_USER, DB_PASS, DB_NAME
- Open your Terminal (Linux) / Command Prompt (Windows)
```
$ cd /path/twitter-get-data
$ npm install
$ npm start 
```
- Open your browser
- Type on address bar http://localhost:3000/

### Author
> Ahmad Ardiansyah