var Twitter = require('twitter');
var express = require('express');

var db = require('../configs/database');
var router = express.Router();

require('dotenv').config();

var client = new Twitter({
  consumer_key: process.env.TWITTER_CONSUMER_KEY,
  consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
  access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
  access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
});

//stream from twitter
client.stream('statuses/filter', {track: 'KAI121'}, function(stream) {
  stream.on('error', function(error) {
    throw error;
  });
	stream.on('data', function(event) {

		var jenis = "";
		if(event.hasOwnProperty('retweeted_status')){
			jenis = "retweet";
		} else {
			jenis = "mention";
		}

		console.log("jenis: ", jenis);
		console.log("user: ", event.user.name);
		console.log("message: ", event.text);

		let sql = "INSERT INTO tweets VALUES (null,'"+event.user.name.toString()+"','"+event.user.screen_name.toString()+"','"
			+jenis.toString()+"','"+event.text.toString().replace(/\'/g, "\"")+"','"+event.created_at.toString()+"','"+event.id_str.toString()+"')";

		db.query(sql, (err, rows) => {
			console.log('inserted from stream.');
		});

  });
});

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Sentimen' });
});

router.get('/list', (req, res, next) => {
	let sql = "SELECT * FROM tweets"
	db.query(sql, (err, rows) => {
		if(err) throw new Error(err);
		res.json({
			error: false,
			num_rows: rows.length,
			results: rows
		});
	})
})

router.get('/tweets', (req, res, next) => {
	client.get('search/tweets', {q: 'KAI121'}, async (error, tweets, response) => {
  	if(error) throw error;

  	let statuses = tweets.statuses;
  	for (var i = 0; i < statuses.length; i++) {
  		// console.log(statuses[i].user.name)
  		var jenis = "";
  		// var message = "";
  		if(statuses[i].hasOwnProperty('retweeted_status')){
  			jenis = "retweet";
  			// message = await escapeString(statuses[i].retweeted_status.text.toString());
  		}
  		else {
  			jenis = "mention";
  			// message = await escapeString(statuses[i].text.toString());
  		}

  		let sqlCheck = "SELECT id_str FROM tweets WHERE id_str = '"+statuses[i].id_str+"'";
  		let countRows = await numRowsData(sqlCheck);
  		if(countRows == 0) {
	  		var sql = "INSERT INTO tweets (id, name, screen_name, category, text, created_at, id_str) "
	  				+" VALUES (null, '"+statuses[i].user.name.toString()+"', '"+statuses[i].user.screen_name.toString()+"', '"+jenis+"', '"
	  				+statuses[i].text.toString()+"', '"+statuses[i].created_at.toString()+"', '"+statuses[i].id_str.toString()+"')";
	  		
	  		console.log(sql);
	  		console.log(jenis);

	  		db.query(sql, (error, rows) => {
	  			if(error) throw new Error(error)
	  			console.log('inserted from search.');		
	  		})
  		}
  	}
		res.json(tweets);
	});
})

function escapeString(paramString) {
	return new Promise((callback, reject) => {
		let stringConvert = paramString.replace(/\\/g, "\\\\")
													.replace(/'/g, "\\'")
													.replace(/"/g, "\\\"");
		callback(stringConvert);
	});
}

function numRowsData(query) {
	return new Promise((callback, reject) => {
		db.query(query, (error, rows) => {
  		if(error) reject(error)

  		callback(rows.length)
  	});
	})
} 

module.exports = router;
